import './App.css'
import Header from './header'
import News from './news'

function App() {

  const articles = [
    {title:"Caitlin Clark's Nike Deal Worth $28M Over Eight Years",descrption:"Caitlin Clark's new deal with Nike is worth up to $28 million over eight years, sources tell the Wall Street Journal. \nNike's original deal with Clark was signed in 2022 and was set to expire at the end of the 23-24 college basketball season. \nEven back in Fe…" ,content:"Caitlin Clark's new deal with Nike is worth up to $28 million over eight years, sources tell the Wall Street Journal. \r\nNike's original deal with Clark was signed in 2022 and was set to expire at the… [+734 chars]",id: 1 },
    {title:	"America’s Cigarette Market Is Up for Grabs", descrption:"Philip Morris will soon be able to sell its most successful smoke-free product in the U.S., setting up a battle with its former parent company Altria.",urlToImage:"https://s.yimg.com/ny/api/res/1.2/UQfgrLkJmiagvz9alqIkQg--/YXBwaWQ9aGlnaGxhbmRlcjt3PTEyMDA7aD02MDA-/https://media.zenfs.com/en/the_wall_street_journal_hosted_996/3e5e63d02711252f3c500e182ba6e1cf",content:	"Americas original Marlboro Man faces a fresh competitor, who also happens to be a Marlboro Man. The showdown between them will reshape the U.S. tobacco market.\r\nMost Read from The Wall Street Journal… [+5145 chars]",id:2},
    {title:"A Day in the Life of a Walmart Manager Who Makes $240,000 a Year",description:	"Nichole Hart walks 20,00…ing is the uncertainty’",urlToImage:"https://images.wsj.net/im-948790/social",content:	"BELLMEAD, TEXASAs night fades to day on a recent morning, Nichole Hart walks briskly into the Walmart store she manages here, just off the freeway a few miles north of Waco. \r\nHer overnight manager, … [+140 chars]",id:3}
  ]

  const listitems = articles.map(article =>
<article id="container" key={article.id}>
          <h2 id="titles">{article.title}</h2>
          <p id="description">{article.description}</p>
          <img id="image" src={article.urlToImage}alt="" />
          <p id="content">{article.content}</p>
        </article>
  )

  return (
    <>
      
     <Header/> 
      <main>
      <span id="name">The wall street journal</span>
      <div className='list'>
      <News/>
      {listitems}
      </div>
     
      </main>
      <footer></footer>
    </>
  )
}

export default App
